﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Web.Framework;
using FluentValidation.Attributes;
using Nop.Plugin.Bs.VendorSystem.Validators;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    [Validator(typeof(VendorLoginValidator))]
    public class VendorLoginModel
    {
        [NopResourceDisplayName("Account.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.Fields.Password")]
        [AllowHtml]
        public string Password { get; set; }

    }
}
