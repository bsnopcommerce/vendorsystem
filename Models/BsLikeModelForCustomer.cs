﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Media;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
  public  class BsLikeModelForCustomer
    {
        public BsLikeModelForCustomer()
        {
            Picture = new PictureModel();
            
        }       
        public int Id { get; set; }
        public string ProductSeName { get; set; }
        public string ProductName { get; set; }
        public PictureModel Picture { get; set; }
    }
}
