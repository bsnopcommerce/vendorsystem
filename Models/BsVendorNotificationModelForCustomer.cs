﻿using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
   public class BsVendorNotificationModelForCustomer
    {
        public BsVendorNotificationModelForCustomer()
        {
            Picture = new PictureModel();

        }
        public int Id { get; set; }
        public int NotifierCustomerId { get; set; }
        public string NotifierCustomerName { get; set; }
        public string NotifierCustomerEmail { get; set; }
        public int NotifyingCustomerId { get; set; }
        public string NotifyingCustomerName { get; set; }
        public string NotifyingCustomerEmail { get; set; }
       public string VendorName { get; set; }
        public int ItemId { get; set; }
        public int ProductId { get; set; }
        public string ProductSeName { get; set; }
        public string ProductName { get; set; }
        public PictureModel Picture { get; set; }

    }
}
