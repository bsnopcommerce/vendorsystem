﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
   public class BsVendorFollowModel
    {
        public int VendorId { get; set; }
        public int FollowCout { get; set; }
        public bool IsFollowedByCurrentCustomer { get; set; }
        public bool IsGuestCustomer { get; set; }

    }
}
