﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class BsVendorNotificationCountModel
    {
        public int CustomerId { get; set; }
        public int TotalNumberOfUnviewNotification { get; set; }
    }
}
