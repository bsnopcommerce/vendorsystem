﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Common;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class BsLikeListModelForCustomer
    {
        public BsLikeListModelForCustomer()
        {
            LikeList = new List<BsLikeModelForCustomer>();
            //PagerModel = new PagerModel();
        }
            public IList<BsLikeModelForCustomer> LikeList { get; set; }
            public PagerModel PagerModel { get; set; }
    }
}
