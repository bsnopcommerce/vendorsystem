﻿using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class BsVendorNotificationListModelForCustomer
    {
        public BsVendorNotificationListModelForCustomer()
        {
            NotificatioList = new List<BsVendorNotificationModelForCustomer>();
        }
        public IList<BsVendorNotificationModelForCustomer> NotificatioList { get; set; }
        public PagerModel PagerModel { get; set; }
        
    }
}
