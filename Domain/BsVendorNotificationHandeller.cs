﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Domain
{
    //public class BsVendorNotificationHandeller
    //{

    //}


    public class BsVendorLikeEvent
    {
        public BsVendorLikeEvent(LikeInfoTable liked)
        {
            this.Leked = liked;
        }
        public LikeInfoTable Leked { get; private set; }
    }

    public class BsVendorDisLikeEvent
    {
        public BsVendorDisLikeEvent(LikeInfoTable liked)
        {
            this.DisLeked = liked;
        }
        public LikeInfoTable DisLeked { get; private set; }
    }

    public class BsVendorFollowEvent
    {
        public BsVendorFollowEvent(BsVendorFollowInfoTable follow)
        {
            this.Follow = follow;
        }
        public BsVendorFollowInfoTable Follow { get; private set; }
    }

    public class BsVendorUnFollowEvent
    {
        public BsVendorUnFollowEvent(BsVendorFollowInfoTable unfollow)
        {
            this.UnFollow = unfollow;
        }
        public BsVendorFollowInfoTable UnFollow { get; private set; }
    }
}
