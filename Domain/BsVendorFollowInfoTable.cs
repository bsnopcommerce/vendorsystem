﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Bs.VendorSystem.Domain
{
   public class BsVendorFollowInfoTable : BaseEntity
    {
        public int CustomerId { get; set; }
        public int VendorId { get; set; }
        public bool IsRegisterdCustomer { get; set; }
        public bool IsFollow { get; set; }

    }
}
