﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Domain
{
    public class BsVendorNotificationInfoTable : BaseEntity
    {
        public int NotifierCustomerId { get; set; }
        public int NotifyingCustomerId { get; set; }
        public int ItemId { get; set; }
        public int ItemTypeId { get; set; }
        public ItemType ItemTypeEnum
        {
            get
            {
                 return (ItemType)this.ItemTypeId;
            }
            set
            {
                this.ItemTypeId = (int)value;
            }
        }
        public DateTime? NotificationCreateDateTimeUtc { get; set; }
        public DateTime? NotificationUpDateTimeUtc { get; set; }
        public bool IsViewed { get; set; }
        
    }


}
