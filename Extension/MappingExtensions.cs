﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Plugin.Bs.VendorSystem.Models;

namespace Nop.Plugin.Bs.VendorSystem.Extension
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return Mapper.Map(source, destination);
        }

        #region
        public static BsVendorInfoTable ToEntity(this VendorRegisterModel model)
        {
            return model.MapTo<VendorRegisterModel, BsVendorInfoTable>();
        }


        public static BsVendorInfoTable ToEntity(this VendorRegisterModel model, BsVendorInfoTable destination)
        {
            return model.MapTo(destination);
        }

        public static VendorRegisterModel ToModel(this  BsVendorInfoTable entity)
        {
            return entity.MapTo<BsVendorInfoTable, VendorRegisterModel>();
        }

        #endregion
    }
}
