﻿using Nop.Services.Logging;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Bs.VendorSystem.Service;

namespace Nop.Plugin.Bs.VendorSystem
{
   public class Scheduler : ITask
    {
        private readonly ILogger _logger;
        private readonly IBsVendorService _bsVendorService;
        public Scheduler(ILogger logger, IBsVendorService bsVendorService)
        {
            this._logger = logger;
            this._bsVendorService = bsVendorService;
        }

        ///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Execute task
		/// </summary>
        public void Execute()
        {
        }



        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
