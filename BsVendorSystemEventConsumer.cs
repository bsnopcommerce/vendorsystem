﻿using Nop.Admin.Models.Catalog;
using Nop.Core;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Plugin.Bs.VendorSystem.Service;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Vendors;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Events;
using Nop.Core.Domain.Vendors;
using Nop.Services.Customers;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Bs.VendorSystem
{
    public class BsVendorSystemEventConsumer : IConsumer<BsVendorLikeEvent>, IConsumer<BsVendorDisLikeEvent>, IConsumer<BsVendorFollowEvent>, IConsumer<BsVendorUnFollowEvent>, IConsumer<EntityInserted<Product>>, IConsumer<EntityInserted<Order>>

    {
        private readonly IBsVendorService _bsVendorService;
        private readonly IVendorService _vendorService;
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public BsVendorSystemEventConsumer(IBsVendorService bsVendorService,
            IVendorService vendorService,
            IProductService productService,
            ICustomerService customerService,
            IOrderService orderService,
            IWorkContext workContext,
            IStoreContext storeContext
            )
        {
            this._bsVendorService = bsVendorService;
            this._vendorService = vendorService;
            this._productService = productService;
            this._customerService = customerService;
            this._orderService = orderService;
            this._workContext = workContext;
            this._storeContext = storeContext;

        }
        public void HandleEvent(BsVendorLikeEvent eventMessage)
        {
            
            var liked = eventMessage.Leked;
            var product = _productService.GetProductById(liked.ProductId);
            var vendorId = product.VendorId;
            var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(vendorId);

            var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
            {
                NotifierCustomerId = liked.CustomerId,
                NotifyingCustomerId = x.CustomerId,
                ItemId = liked.ProductId,
                ItemTypeId = 100,
                NotificationCreateDateTimeUtc = DateTime.UtcNow,
                IsViewed = false
            }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

            try
            {
                _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
            }
            catch (Exception ex)
            {
                throw;
            }


        }

        public void HandleEvent(BsVendorDisLikeEvent eventMessage)
        {
            var disLiked = eventMessage.DisLeked;
            var product = _productService.GetProductById(disLiked.ProductId);
            var vendorId = product.VendorId;
            var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(vendorId);

            var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
            {
                NotifierCustomerId = disLiked.CustomerId,
                NotifyingCustomerId = x.CustomerId,
                ItemId = disLiked.ProductId,
                ItemTypeId = 200,
                NotificationCreateDateTimeUtc = DateTime.UtcNow,
                IsViewed = false
            }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

            try
            {
                _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public void HandleEvent(BsVendorFollowEvent eventMessage)
        {
            var follow = eventMessage.Follow;
            var vendorId = follow.VendorId;
            var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(vendorId);

            var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
            {
                NotifierCustomerId = follow.CustomerId,
                NotifyingCustomerId = x.CustomerId,
                ItemId = follow.VendorId,
                ItemTypeId = 300,
                NotificationCreateDateTimeUtc = DateTime.UtcNow,
                IsViewed = false
            }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

            try
            {
                _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public void HandleEvent(BsVendorUnFollowEvent eventMessage)
        {
            var unFollow = eventMessage.UnFollow;
            var vendorId = unFollow.VendorId;
            var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(vendorId);
            var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
            {
                NotifierCustomerId = unFollow.CustomerId,
                NotifyingCustomerId = x.CustomerId,
                ItemId = unFollow.VendorId,
                ItemTypeId = 400,
                NotificationCreateDateTimeUtc = DateTime.UtcNow,
                IsViewed = false
            }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

            try
            {
                _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
            }
            catch (Exception ex)
            {
                throw;
            }
          
        }

        public void HandleEvent(EntityInserted<Product> eventMessage)
        {
            
            if (eventMessage.Entity.VendorId > 0)
            {
                var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(eventMessage.Entity.VendorId);
                var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
                {
                    NotifierCustomerId = eventMessage.Entity.VendorId,
                    NotifyingCustomerId = x.CustomerId,
                    ItemId = x.VendorId,
                    ItemTypeId = 500,
                    NotificationCreateDateTimeUtc = DateTime.UtcNow,
                    IsViewed = false
                }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

                try
                {
                    _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                var adminCustomerRoll = _bsVendorService.GetAllAdminCustomerRole();
                var customerRoll = new int[] { adminCustomerRoll.Id };
                var adminCustomerList = _bsVendorService.GetAllAdminCustomerByRollId(customerRoll);

                var vendorsToSentNotificationAdmin = adminCustomerList.Select(x => new BsVendorNotificationInfoTable
                {
                    NotifierCustomerId = eventMessage.Entity.VendorId,
                    NotifyingCustomerId = x.Id,
                    ItemId = eventMessage.Entity.Id,
                    ItemTypeId = 500,
                    NotificationCreateDateTimeUtc = DateTime.UtcNow,
                    IsViewed = false
                }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

                try
                {
                    _bsVendorService.InsertVendorNotification(vendorsToSentNotificationAdmin);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
          
        }

        public void HandleEvent(EntityInserted<Order> eventMessage)
        {

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var productList = new List<Product>();
            foreach (var shoppingCartItem in cart)
            {
                productList.Add(shoppingCartItem.Product);
            }
            foreach (var product in productList)
            {
                if (product.VendorId > 0)
                {
                    
                    var folowerLst = _bsVendorService.GetFollowCustomerByVendorId(product.VendorId);
                   
                        var vendorsToSentNotification = folowerLst.Select(x => new BsVendorNotificationInfoTable
                        {
                            NotifierCustomerId = eventMessage.Entity.CustomerId,
                            NotifyingCustomerId = x.CustomerId,
                            ItemId = product.Id,
                            ItemTypeId = 600,
                            NotificationCreateDateTimeUtc = DateTime.UtcNow,
                            IsViewed = false
                        }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);

                        try
                        {
                            _bsVendorService.InsertVendorNotification(vendorsToSentNotification);
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }
                else
                {
                    var adminCustomerRoll = _bsVendorService.GetAllAdminCustomerRole();
                    var customerRoll = new int[] { adminCustomerRoll.Id };
                    var adminCustomerList = _bsVendorService.GetAllAdminCustomerByRollId(customerRoll);
                    var vendorsToSentNotificationAdmin = adminCustomerList.Select(x => new BsVendorNotificationInfoTable
                    {
                        NotifierCustomerId = eventMessage.Entity.CustomerId,
                        NotifyingCustomerId = x.Id,
                        ItemId = product.Id,
                        ItemTypeId = 600,
                        NotificationCreateDateTimeUtc = DateTime.UtcNow,
                        IsViewed = false
                    }).Where(x => x.NotifierCustomerId != x.NotifyingCustomerId);
                    try
                    {
                        _bsVendorService.InsertVendorNotification(vendorsToSentNotificationAdmin);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
        }
    }
}
