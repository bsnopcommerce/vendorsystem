﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Bs.VendorSystem.Data;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Plugin.Bs.VendorSystem.Service;

namespace Nop.Plugin.Bs.VendorSystem.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        private const string CONTEXT_NAME = "nop_object_context_vendor";
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<BsVendorService>().As<IBsVendorService>().InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<BsVendorObjectContext>(builder, CONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<BsVendorInfoTable>>()
                .As<IRepository<BsVendorInfoTable>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<LikeInfoTable>>()
   .As<IRepository<LikeInfoTable>>()
   .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
   .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BsVendorFollowInfoTable>>()
             .As<IRepository<BsVendorFollowInfoTable>>()
             .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
             .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BsVendorNotificationInfoTable>>()
            .As<IRepository<BsVendorNotificationInfoTable>>()
            .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
            .InstancePerLifetimeScope();
        }

        public int Order
        {
            get { return 1; }
        }

        
    }
}
