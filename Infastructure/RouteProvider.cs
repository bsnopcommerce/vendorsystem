﻿using Nop.Web.Framework.Mvc.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using Nop.Web.Framework.Localization;
namespace Nop.Plugin.Bs.VendorSystem.Infastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            
            routes.MapLocalizedRoute("Vendor.Registration",
                    "Vendor/Registration",
                    new { controller = "BsVendor", action = "VendorRegistration" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.Approve",
                    "BsVendor/VendorApprove/{id}",
                    new { controller = "BsVendor", action = "VendorApprove" },
                    new { id = @"\d+" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.MarketPlace",
                    "MarketPlace",
                    new { controller = "BsVendor", action = "MarketPlace" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("VendorHomePage",
                    "vendor",
                    new { controller = "BsVendor", action = "index" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.Login",
                    "Vendor/Login",
                    new { controller = "BsVendor", action = "VendorLogin" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.List",
                    "Vendor/ProductList",
                    new { controller = "BsVendor", action = "List" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.Create",
                    "Vendor/ProductCreate",
                    new { controller = "BsVendor", action = "Create" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            //get state list by country ID  (AJAX link)
            routes.MapLocalizedRoute("GetAllSubCategoriesByParentCategoryId",
                    "Vendor/GetAllSubCategoriesByParentCategoryId/",
                    new { controller = "BsVendor", action = "GetAllSubCategoriesByParentCategoryId" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.Edit",
                    "Vendor/ProductEdit/{id}",
                    new { controller = "BsVendor", action = "Edit", id = "" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.OrderList",
                    "Vendor/OrderList",
                    new { controller = "BsVendor", action = "OrderList" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.OrderEdit",
                    "Vendor/OrderEdit/{id}",
                    new { controller = "BsVendor", action = "OrderEdit", id = "" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.ShipmentList",
                    "Vendor/ShipmentList",
                    new { controller = "BsVendor", action = "ShipmentList" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.BestsellersReport",
                    "Vendor/BestsellersReport",
                    new { controller = "BsVendor", action = "BestsellersReport" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.NeverSoldReport",
                    "Vendor/NeverSoldReport",
                    new { controller = "BsVendor", action = "NeverSoldReport" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            //like route//
            routes.MapLocalizedRoute("Vendor.CustomerProductLike",
                    "VendorProductLike/{productId}",
                    new { controller = "BsVendor", action = "ProductLike" },
                    new { productId = @"\d+" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.CustomerProductUnLike",
                    "VendorProductUnlike/{productId}",
                    new { controller = "BsVendor", action = "ProductUnLike" },
                    new { productId = @"\d+" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.LikelistForCustomer",
                    "VendorLike",
                    new { controller = "BsVendor", action = "LikeListForCustomer" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            //follow route//

            routes.MapLocalizedRoute("Vendor.CustomerVendorFollow",
                    "VendorFollow/{vendorId}",
                    new { controller = "BsVendor", action = "VendorFollow" },
                    new { vendorId = @"\d+" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.CustomerVendorUnFollow",
                    "VendorUnFollow/{vendorId}",
                    new { controller = "BsVendor", action = "VendorUnFollow" },
                    new { vendorId = @"\d+" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            routes.MapLocalizedRoute("Vendor.FollowListForCustomer",
                    "VendorFollow",
                    new { controller = "BsVendor", action = "FollowListForCustomer" },
                    new[] { "Nop.Plugin.Bs.VendorSystem" });

            //Notification Route

            routes.MapLocalizedRoute("Vendor.UnViewNotificationList",
        "UnViewNotificationList",
        new { controller = "BsVendor", action = "UnViewNotificationList" },
        new[] { "Nop.Plugin.Bs.VendorSystem" });

          routes.MapLocalizedRoute("Vendor.AllNotificationList",
        "AllNotificationList",
        new { controller = "BsVendor", action = "AllNotificationList" },
        new[] { "Nop.Plugin.Bs.VendorSystem" });



            routes.MapLocalizedRoute(
                "UpdateNotificationByCustomer",
                "makenotificationviewed",
                new { controller = "BsVendor", action = "UpdateNotificationByCustomer", area = "", id = "" },
                new[] { "Nop.Plugin.Bs.VendorSystem" });
         


        }
        public int Priority
        {
            get
            {
                return -1;
            }
        }
    }
}
