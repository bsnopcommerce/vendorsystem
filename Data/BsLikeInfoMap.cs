﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data.Mapping;
using Nop.Plugin.Bs.VendorSystem.Domain;

namespace Nop.Plugin.Bs.VendorSystem.Data
{

    public class BsLikeInfoMap : NopEntityTypeConfiguration<LikeInfoTable>
    {
        public BsLikeInfoMap()
        {
            this.ToTable("BsLikeTable");
            this.HasKey(x => x.Id);
        }
    }
}
